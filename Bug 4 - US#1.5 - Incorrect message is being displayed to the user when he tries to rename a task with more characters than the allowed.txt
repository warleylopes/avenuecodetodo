Incorrect message is being displayed to the user when he tries to rename a task with more characters than the allowed

Precondition: User is logged in and is at the My Tasks page.

1. Type more than 254 characters when renaming an already created task.
2. Press enter.

Expected: A message saying that the user can only type up to 250 characters should be displayed.

Actual: The message "Task can't be blank!" is being displayed incorrectly.

Low