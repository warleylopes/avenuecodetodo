package com.qatest.avenuecode.test;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/com/qatest/avenuecode/test/feature", glue = { "com.qatest.avenuecode.test.stepDefinitions" })

public class TestRunner {
	//Run this class as a JUnit Test
}
