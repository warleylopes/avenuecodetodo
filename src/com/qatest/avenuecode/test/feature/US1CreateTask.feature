Feature: Create Task
	As a ToDo App user
	I should be able to create a task
	So I can manage my tasks

#Covers US#1 acceptance criterea and bugs from 1 to 4
Scenario Outline: Creating tasks
	Given User is logged in
	When  User clicks on My Tasks link
	Then  User should see his ToDo list
	
	When  User creates new task by plus button
	Then  Task should be created
	When  User clicks to edit task
	And   User, in task editing field, types "First"
	And   User submit the editing by clicking
	Then  Task should be renamed
	
	When  User clicks to remove a task
	Then  Task should be removed
	
	When  User types new task <ToDo>
	And   User submit new task
	Then  Task should be <be>
	And   User closes the browser
	
	Examples: Task ToDo entering possibilities
	|ToDo						|be				|
	|"New Task"					|created		|
	|"nothing"					|notCreated		|
	|"ab"						|notCreated		|
	|"${251}"					|notCreated		|

Scenario Outline: Task editing
	Given User is logged in
	When  User clicks on My Tasks link	
	And   User clicks to edit task
	And   User, in task editing field, types <ToDo>
	And   User submit the editing by <by>
	Then  Task should be <be>
	And   Task editing failed message displayed is <is>
	And   User closes the browser
	
	Examples: Task ToDo editing possibilities
	|ToDo			|by			|be				|is							|
	|"nothing"		|enter		|notRenamed		|"Task can't be blank!"		|
	|"a"			|clicking	|notRenamed		|"Task can't be blank!"		|
	|"${255}"		|enter		|notRenamed		|"250 characters maximum"	|
	|"New Task"		|enter		|renamed		|"nothing"					|