Feature: Create SubTask
	As a ToDo App user
	I should be able to create a subtask
	So I can break down my tasks in smaller pieces

#Covers US#2 acceptance criteria and bugs from 5 to 9
Scenario Outline: Creating subtasks
	Given User is logged in
	When  User clicks on My Tasks link
	Then  User should see his ToDo list
	And   Users should see button which label ends with ") Manage Subtasks"
	
	When  Users clicks on Manage Subtasks button
	Then  Modal dialog should pop up
	And   Modal dialog popup should have a read-only task ID field and a task description
	And   Subtask due date should be in the format expected
	
	When  User types new subtask <taskDesc> <subDesc> and <dueDate>
	And   User submit new subtask by <by>
	Then  Subtask should be <be>
	And   User closes the browser
	
	#Bug 6,8 and 9 tests
	Examples: Subtask creation possibilities
	|taskDesc		|subDesc		|dueDate		|by						|be				|
	|"nothing"		|"nothing"		|"nothing"		|entering				|created		|
	|"nothing"		|"anything"		|"12/15/2015"	|entering				|notCreated		|
	|"anything"		|"anything"		|"nothing"		|clicking				|notCreated		|
	|"anything"		|"anything"		|"12/15/2015"	|entering				|created		|
	|"anything"		|"anything"		|"15/12/2015"	|clicking				|notCreated		|
	|"anything"		|"${251}"		|"12/15/2015"	|entering				|notCreated		|
	
Scenario Outline: Message test when renaming subtask
	Given User is logged in
	When  User clicks on My Tasks link
	And   Users clicks on Manage Subtasks button
	And   User types new subtask "anything" "anything" and "12/20/2020"
	And   User submit new subtask by clicking
	Then  Subtask should be created
	When  User edits subtask with <desc>
	Then  Subtask should be <be>
	And   Subtask editing failed message displayed is <is>
	And   User closes the browser
	
	Examples: Subtask editing
	|desc					|be									|is							|
	|"${255}"				|notRenamed							|"250 characters maximum"	|