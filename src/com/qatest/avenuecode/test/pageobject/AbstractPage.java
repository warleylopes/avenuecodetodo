package com.qatest.avenuecode.test.pageobject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class AbstractPage {

	protected WebDriver driver;

	AbstractPage() {
		getDriver();
	}

	protected WebDriver getDriver() {
		if (driver == null) {
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}
		return driver;
	}

	public void closeBrowser() {
		driver.close();
	}

	public void navigateToHomePage() {
		driver.get("http://qa-test.avenuecode.com/");
	}

	public void signIn() {
		if (driver.getTitle().equals("ToDo Rails and Angular")) {
			if (driver.findElement(By.linkText("Sign In")).isDisplayed()) {
				driver.findElement(By.linkText("Sign In")).click();
				driver.findElement(By.id("user_email")).sendKeys("warleylopes@outlook.com");
				driver.findElement(By.id("user_password")).sendKeys("cDgprsZFZQ!WaS");
				driver.findElement(By.id("user_remember_me")).click();
				driver.findElement(By.name("commit")).click();
			}
		}
	}
	
	public boolean isLoggedIn(){
		if (driver.findElement(By.className("alert")).isDisplayed()){
			return true;
		}else{
			return false;
		}
	}
	
	public void navigateToMyTasks() {
		// Navigate to My Tasks page.
		driver.findElement(By.linkText("My Tasks")).click();
	}
	
	public boolean isInMyTasksPage(){
		return driver.getCurrentUrl().equals("http://qa-test.avenuecode.com/tasks");
	}
	
	public boolean isToDoListMsgDisplayed(){
		return driver.findElement(By.xpath("/html/body/div[1]/h1")).getText().toString().endsWith(" ToDo List");
	}
}
