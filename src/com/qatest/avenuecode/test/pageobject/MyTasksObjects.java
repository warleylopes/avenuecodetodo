package com.qatest.avenuecode.test.pageobject;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;

public class MyTasksObjects extends AbstractPage {
	
	String modalTitle;

	public void clicksOnNewTaskPlusButton() {
		driver.findElement(By.className("glyphicon-plus")).click();
	}

	public int getNumberOfTasksCreated() {
		return driver.findElements(By.className("editable-click")).size();
	}

	public void clicksOnCreatedTaskToDo() {
		driver.findElements(By.className("editable-click")).get(0).click();
	}

	public void typesInTaskToDoEditing(String taskToDo) {
		if (taskToDo.startsWith("$")) {
			taskToDo = RandomStringUtils.randomAlphabetic(Integer.parseInt(taskToDo.substring(2, 5)));
		}
		if (taskToDo.equals("nothing")){
			taskToDo = "";
		}
		driver.findElements(By.className("editable-input")).get(0).sendKeys(taskToDo);
	}

	public void submitInTaskToDoEditingClicking() {
		driver.findElements(By.className("editable-buttons")).get(0).click();
	}

	public void submitInTaskToDoEditingEnterKey() {
		driver.findElements(By.className("editable-input")).get(0).submit();
	}

	public boolean didAnErrorOccurredWhileEditing() {
		return driver.findElements(By.className("editable-error")).get(0).isDisplayed();
	}

	public void clicksOnRemoveTask() {
		driver.findElements(By.className("btn-danger")).get(0).click();
	}

	public void typesNewTask(String arg1) {
		driver.findElement(By.id("new_task")).click();
		driver.findElement(By.id("new_task")).clear();
		if (arg1.startsWith("$")) {
			arg1 = RandomStringUtils.randomAlphabetic(Integer.parseInt(arg1.substring(2, 5)));
		}
		if (arg1.equals("nothing")){
			arg1 = "";
		}
		driver.findElement(By.id("new_task")).sendKeys(arg1);
	}

	public void submitNewTaskByEnter() {
		driver.findElement(By.id("new_task")).submit();
	}

	public boolean isEditingErrorMessageSomethingLike(String msg) {
		if(msg.startsWith("[0-9]")){
			msg = msg.substring(0,3);
		}
		return driver.findElements(By.className("editable-error")).get(0).getText().contains(msg);
	}

	public boolean isTheManageSubTaskButtonEndsWith(String lastPartOfTheLabel) {
		return driver.findElements(By.className("btn-primary")).get(0).getText().endsWith(lastPartOfTheLabel);
	}

	public void clicksOnManageSubTasks() {
		driver.findElements(By.className("btn-primary")).get(0).click();
	}

	public boolean isThereAModalPopup() {
		return driver.findElements(By.className("modal-dialog")).get(0).isDisplayed();
	}

	public boolean isThereATaskIDField() throws InterruptedException {
		modalTitle = driver.findElement(By.className("modal-title")).getText();
		if(modalTitle == null || modalTitle.length() < 13){
			wait(10000);
			modalTitle = driver.findElement(By.className("modal-title")).getText();
			if(modalTitle.substring(13).matches("\\d+")){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public boolean isThereATaskDescField() {
		return driver.findElement(By.id("edit_task")).isDisplayed();
	}

	public boolean isThereASubTaskDescField() {
		return driver.findElement(By.id("new_sub_task")).isDisplayed();
	}

	public boolean isThereASubTaskDueDateField() {
		return driver.findElement(By.id("dueDate")).isDisplayed();
	}

	public boolean isTheSubTaskDueDateFieldInExpectedFormat() {
		return driver.findElements(By.id("dueDate")).get(0).getText().matches("\\d{2}-\\d{2}-\\d{4}");
	}

	public void typesTaskToDoInsideSubTasksModalPopup(String taskDesc) {
		driver.findElement(By.id("edit_task")).clear();
		if (taskDesc.startsWith("$")) {
			taskDesc = RandomStringUtils.randomAlphabetic(Integer.parseInt(taskDesc.substring(2, 5)));
		}
		if (taskDesc.equals("nothing")){
			taskDesc = "";
		}
		driver.findElement(By.id("edit_task")).sendKeys(taskDesc);
	}

	public void typesSubTaskDesc(String subDesc) {
		driver.findElement(By.id("new_sub_task")).clear();
		if (subDesc.startsWith("$")) {
			subDesc = RandomStringUtils.randomAlphabetic(Integer.parseInt(subDesc.substring(2, 5)));
		}
		if (subDesc.equals("nothing")){
			subDesc = "";
		}
		driver.findElement(By.id("new_sub_task")).sendKeys(subDesc);
	}

	public void typesSubTaskDueDate(String dueDate) {
		driver.findElement(By.id("dueDate")).clear();
		if (dueDate.equals("nothing")){
			dueDate = "";
		}
		driver.findElement(By.id("dueDate")).sendKeys(dueDate);
	}

	public int getNumberOfSubTasksCreated() {
		return driver.findElements(By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/a")).size();
	}

	public void submitNewSubTaskByClicking() {
		driver.findElement(By.id("add-subtask")).click();
	}

	public void submitNewSubTaskByEnter() {
		driver.findElement(By.id("new_sub_task")).submit();
	}

	public void clicksOnCreatedSubTask() {
		driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/a")).click();
	}

	public void typesInSubTaskEditing(String subDesc) {
		driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/form/div/input"))
				.clear();
		if (subDesc.startsWith("$")) {
			subDesc = RandomStringUtils.randomAlphabetic(Integer.parseInt(subDesc.substring(2, 5)));
		}
		if (subDesc.equals("nothing")){
			subDesc = "";
		}
		driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/form/div/input"))
				.sendKeys(subDesc);
	}

	public void submitSubTaskEditingByEnter() {
		driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/form/div/input"))
				.submit();
	}
}
