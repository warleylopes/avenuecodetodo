package com.qatest.avenuecode.test.stepDefinitions;

import org.openqa.selenium.remote.SessionNotFoundException;
import org.testng.Assert;

import com.qatest.avenuecode.test.pageobject.MyTasksObjects;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MyTasksSteps {

	MyTasksObjects myTasksObjects = new MyTasksObjects();
	int tasksNumberBefore = 0, tasksNumberAfter = 0, subTasksNumberBefore = 0, subTasksNumberAfter = 0;

	@Given("^User is logged in$")
	public void user_is_logged_in() {
		try {
			myTasksObjects.navigateToHomePage();
			myTasksObjects.signIn();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
		try {
			Assert.assertTrue(myTasksObjects.isLoggedIn(),
					"UNEXPECTED BEHAVIOR: For some reason, the user wasn't logged in!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@When("^User clicks on My Tasks link$")
	public void user_clicks_on_My_Tasks_link() {
		try {
			myTasksObjects.navigateToMyTasks();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
		try {
			Assert.assertTrue(myTasksObjects.isInMyTasksPage(),
					"UNEXPECTED BEHAVIOR: For some reason, the user wan't in My Tasks Page!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@Then("^User should see his ToDo list$")
	public void user_should_see_his_ToDo_list() {
		try {
			Assert.assertTrue(myTasksObjects.isToDoListMsgDisplayed(),
					"UNEXPECTED BEHAVIOR: For some reason, the ToDo list belong to logged in user message wasn't displayed!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@When("^User creates new task by plus button$")
	public void user_creates_new_task_by_plus_button() {
		try {
			tasksNumberBefore = myTasksObjects.getNumberOfTasksCreated();
			myTasksObjects.clicksOnNewTaskPlusButton();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
	}

	@Then("^Task should be created$")
	public void task_should_be_created() {
		try {
			tasksNumberAfter = myTasksObjects.getNumberOfTasksCreated();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
		try {
			Assert.assertTrue(tasksNumberBefore < tasksNumberAfter,
					"UNEXPECTED BEHAVIOR: For some reason, a task wasn't created!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@When("^User clicks to edit task$")
	public void user_clicks_to_edit_task() {
		try {
			myTasksObjects.clicksOnCreatedTaskToDo();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
	}

	@When("^User, in task editing field, types \"([^\"]*)\"$")
	public void user_in_task_editing_field_types(String arg1) {
		try {
			myTasksObjects.typesInTaskToDoEditing(arg1);
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
	}

	@Then("^Task should be renamed$")
	public void task_should_be_renamed() {
		try {
			Assert.assertTrue(myTasksObjects.didAnErrorOccurredWhileEditing(),
					"UNEXPECTED BEHAVIOR: For some reason, a task wasn't renamed!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@When("^User clicks to remove a task$")
	public void user_clicks_to_remove_a_task() {
		try {
			tasksNumberBefore = myTasksObjects.getNumberOfTasksCreated();
			myTasksObjects.clicksOnRemoveTask();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
	}

	@Then("^Task should be removed$")
	public void task_should_be_removed() {
		try {
			tasksNumberAfter = myTasksObjects.getNumberOfTasksCreated();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
		try {
			Assert.assertTrue(tasksNumberBefore > tasksNumberAfter,
					"UNEXPECTED BEHAVIOR: For some reason, a task wasn't removed!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@When("^User types new task \"([^\"]*)\"$")
	public void user_types_new_task(String arg1) {
		try {
			myTasksObjects.typesNewTask(arg1);
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
	}

	@When("^User submit new task$")
	public void user_submit_new_task() {
		try {
			myTasksObjects.submitNewTaskByEnter();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the browser was already closed
		}
	}

	@Then("^User closes the browser$")
	public void user_closes_the_browser() {
		try {
			myTasksObjects.closeBrowser();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the browser was already closed
		}
	}

	@Then("^Task should be notCreated$")
	public void task_should_be_notCreated() {
		try {
			tasksNumberAfter = myTasksObjects.getNumberOfTasksCreated();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
		try {
			Assert.assertTrue(tasksNumberBefore == tasksNumberAfter,
					"UNEXPECTED BEHAVIOR: For some reason, a tasks number didn't stay the same!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@When("^User submit the editing by enter$")
	public void user_submit_the_editing_by_enter() {
		try {
			myTasksObjects.submitInTaskToDoEditingEnterKey();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
	}

	@Then("^Task should be notRenamed$")
	public void task_should_be_notRenamed() {
		try {
			Assert.assertFalse(myTasksObjects.didAnErrorOccurredWhileEditing(),
					"UNEXPECTED BEHAVIOR: For some reason, a task was renamed improperly!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@Then("^Task editing failed message displayed is \"([^\"]*)\"$")
	public void task_editing_failed_message_displayed_is(String msg) {
		try {
			Assert.assertTrue(myTasksObjects.isEditingErrorMessageSomethingLike(msg),
					"UNEXPECTED BEHAVIOR: For some reason, the editing failed message displayed wasn't right!!!");
		} catch (SessionNotFoundException | AssertionError e) {
			if (e.getMessage().contains("UNEXPECTED BEHAVIOR")) {
				String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
						e.getMessage().indexOf("!!!") + 3));
				//e.printStackTrace();
				myTasksObjects.closeBrowser();
				Assert.fail(error);
			}
		}
	}

	@When("^User submit the editing by clicking$")
	public void user_submit_the_editing_by_clicking() {
		try {
			myTasksObjects.submitInTaskToDoEditingClicking();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
	}

	@Then("^Users should see button which label ends with \"([^\"]*)\"$")
	public void users_should_see_button_which_label_ends_with(String labelEnding) {
		try {
			Assert.assertTrue(myTasksObjects.isTheManageSubTaskButtonEndsWith(labelEnding),
					"UNEXPECTED BEHAVIOR: For some reason, the Manage SubTask button wasn't displayed!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@When("^Users clicks on Manage Subtasks button$")
	public void users_clicks_on_Manage_Subtasks_button() {
		try {
			myTasksObjects.clicksOnManageSubTasks();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
	}

	@Then("^Modal dialog should pop up$")
	public void modal_dialog_should_pop_up() {
		try {
			Assert.assertTrue(myTasksObjects.isThereAModalPopup(),
					"UNEXPECTED BEHAVIOR: For some reason, the Manage SubTasks modal dialog didn't pop up!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@Then("^Modal dialog popup should have a read-only task ID field and a task description$")
	public void modal_dialog_popup_should_have_a_read_only_task_ID_field_and_a_task_description() throws InterruptedException {
		try {
			Assert.assertTrue(myTasksObjects.isThereATaskIDField(),
					"UNEXPECTED BEHAVIOR: For some reason, a Task ID field wasn't displayed!!!");
			Assert.assertTrue(myTasksObjects.isThereATaskDescField(),
					"UNEXPECTED BEHAVIOR: For some reason, a Task description field wasn't displayed!!!");
			Assert.assertTrue(myTasksObjects.isThereASubTaskDescField(),
					"UNEXPECTED BEHAVIOR: For some reason, a SubTask description field wasn't displayed!!!");
			Assert.assertTrue(myTasksObjects.isThereASubTaskDueDateField(),
					"UNEXPECTED BEHAVIOR: For some reason, a SubTask due date field wasn't displayed!!!");
		} catch (AssertionError e) {
			if (e.getMessage().contains("UNEXPECTED BEHAVIOR")) {
				String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
						e.getMessage().indexOf("!!!") + 3));
				//e.printStackTrace();
				myTasksObjects.closeBrowser();
				Assert.fail(error);
			}
		}
	}

	@Then("^Subtask due date should be in the format expected$")
	public void subtask_due_date_should_be_in_the_format_expected() {
		try {
			Assert.assertTrue(myTasksObjects.isTheSubTaskDueDateFieldInExpectedFormat(),
					"UNEXPECTED BEHAVIOR: For some reason, a SubTask Due Date field wasn't in the right format!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@When("^User types new subtask \"([^\"]*)\" \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_types_new_subtask_and(String taskDesc, String subDesc, String dueDate) {
		try {
			myTasksObjects.typesTaskToDoInsideSubTasksModalPopup(taskDesc);
			myTasksObjects.typesSubTaskDesc(subDesc);
			myTasksObjects.typesSubTaskDueDate(dueDate);
		} catch (SessionNotFoundException s) {
			// Do nothing here because the browser was already closed
		}
	}

	@When("^User submit new subtask by entering$")
	public void user_submit_new_subtask_by_entering() {
		try {
			subTasksNumberBefore = myTasksObjects.getNumberOfSubTasksCreated();
			myTasksObjects.submitNewSubTaskByEnter();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the browser was already closed
		}
	}

	@Then("^Subtask should be created$")
	public void subtask_should_be_created() {
		try {
			subTasksNumberAfter = myTasksObjects.getNumberOfSubTasksCreated();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
		try {
			Assert.assertTrue(subTasksNumberBefore < subTasksNumberAfter,
					"UNEXPECTED BEHAVIOR: For some reason, a SubTask wasn't created!!!");
		} catch (AssertionError e) {
			if (e.getMessage().contains("UNEXPECTED BEHAVIOR")) {
				String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
						e.getMessage().indexOf("!!!") + 3));
				//e.printStackTrace();
				myTasksObjects.closeBrowser();
				Assert.fail(error);
			}
		}
	}

	@Then("^Subtask should be notCreated$")
	public void subtask_should_be_notCreated() {
		try {
			subTasksNumberAfter = myTasksObjects.getNumberOfSubTasksCreated();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
		try {
			Assert.assertTrue(subTasksNumberBefore == subTasksNumberAfter,
					"UNEXPECTED BEHAVIOR: For some reason, a SubTask was created improperly!!!");
		} catch (AssertionError e) {
			if (e.getMessage().contains("UNEXPECTED BEHAVIOR")) {
				String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
						e.getMessage().indexOf("!!!") + 3));
				//e.printStackTrace();
				myTasksObjects.closeBrowser();
				Assert.fail(error);
			}
		}
	}

	@When("^User submit new subtask by clicking$")
	public void user_submit_new_subtask_by_clicking() {
		try {
			subTasksNumberBefore = myTasksObjects.getNumberOfSubTasksCreated();
			myTasksObjects.submitNewSubTaskByClicking();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the browser was already closed
		}
	}

	@When("^User edits subtask with \"([^\"]*)\"$")
	public void user_edits_subtask_with(String subDesc) {
		try {
			myTasksObjects.clicksOnCreatedSubTask();
			myTasksObjects.typesInSubTaskEditing(subDesc);
			myTasksObjects.submitSubTaskEditingByEnter();
		} catch (SessionNotFoundException s) {
			// Do nothing here because the webdriver was already closed
		}
	}

	@Then("^Subtask should be notRenamed$")
	public void subtask_should_be_notRenamed() {
		try {
			Assert.assertTrue(myTasksObjects.didAnErrorOccurredWhileEditing(),
					"UNEXPECTED BEHAVIOR: For some reason, a SubTask was renamed improperly!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}

	@Then("^Subtask editing failed message displayed is \"([^\"]*)\"$")
	public void subtask_editing_failed_message_displayed_is(String msg) {
		try {
			Assert.assertTrue(myTasksObjects.isEditingErrorMessageSomethingLike(msg),
					"UNEXPECTED BEHAVIOR: For some reason, a SubTask editing failed message displayed wasn't right!!!");
		} catch (AssertionError e) {
			String error = (e.getMessage().substring(e.getMessage().indexOf("UNEXPECTED BEHAVIOR"),
					e.getMessage().indexOf("!!!") + 3));
			//e.printStackTrace();
			myTasksObjects.closeBrowser();
			Assert.fail(error);
		}
	}
}